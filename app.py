import os
from typing import List
import wx
import hashlib
import threading
import queue
import humanize

import wxml
import utils

class FileDrop(wx.FileDropTarget):
    def __init__(self, subscribe=None):
        super().__init__()
        self.on_drop = wxml.Event('on_drop')
        if subscribe is not None:
            self.on_drop += subscribe

    def OnDropFiles(self, x, y, filenames):
        self.on_drop(filenames)
        return True

@wxml.Ui('ui.xml')
class HasherUi(wxml.ViewModel):
    def initialize(self):
        self.drop_handler = FileDrop()
        self.drop_handler.on_drop += self.queue_hash

        self.choices = wxml.ArrayBindValue(
            sorted(['md5', 'sha1', 'sha256', 'sha512']),
            default=0,
            trace=False,
            name='hashDisplay',
            serialize=False)

        self.hash_queue = queue.Queue()
        self.stop_thread = threading.Event()
        self.hash_thread = threading.Thread(target=self._hasher)
        self.hashed_files : List[utils.MultiHash] = []

        self.on_close += self.cleanup

    def ready(self):
        self.choices.item.value_changed += self.update_column
        self.choices.item.value_changed += self.update_displayed_hash

        self.update_column(self.choices.item.value)

        self.hash_thread.start()

    def update_column(self, value):
        lc : wx.ListCtrl = self.view.widgets['list']
        column : wx.ListItem  = lc.GetColumn(2)
        column.SetText(value)
        lc.SetColumn(2, column)

    def update_displayed_hash(self, value):
        lc : wx.ListCtrl = self.view.widgets['list']
        for idx, h in enumerate(self.hashed_files):
            d = h.digests[value]
            lc.SetItem(idx, 2, d)

    def echo(self, v):
        print(v)

    def to_str(self, v):
        print('to_str')
        return [f.__name__ for f in v]

    def cleanup(self):
        self.stop_thread.set()
        self.hash_thread.join()

    def show_menu(self, evt):
        self.view.PopupMenu(self.view.widgets['menu'])

    def _hasher(self):
        while not self.stop_thread.is_set():
            try:
                filename, bind = self.hash_queue.get(timeout=0.5)

                hashed = utils.Multihash()

                file_length = os.path.getsize(filename)
                processed = 0

                with open(filename, 'rb') as fp:
                    for c in iter(lambda: fp.read(4096), b''):
                        if self.stop_thread.is_set():
                            break

                        hashed.update(c)
                        processed += len(c)
                        bind.value = 'hashing... %.f%%' % (100.0 * processed / file_length)
                    else:
                        bind.value = hashed.digests[self.choices.item.value]
                        self.hashed_files.append(hashed)
            except queue.Empty:
                pass

    @wxml.block_ui
    def set_font(self, value):
        if value.startswith('hashing'):
            return

    @wxml.block_ui
    def put_results(self, name):
        w : wx.ListCtrl = self.view.widgets['list']
        item = w.InsertItem(w.ItemCount, name)

        fsize = os.path.getsize(name)
        w.SetItem(index=item, column=1, label=humanize.naturalsize(fsize))

        bind_value = wxml.BindValue('queued...')
        bind_value.add_target2(w, w.SetItem, index=item, column=2, label=bind_value)
        bind_value.touch()

        return bind_value

    def queue_hash(self, files):
        for f in files:
            value = self.put_results(f)
            self.hash_queue.put((f, value))

if __name__ == "__main__":
    wxml.run(HasherUi)