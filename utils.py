import hashlib

class Multihash(object):
    def __init__(self):
        self.hashers = [
            hashlib.md5(),
            hashlib.sha1(),
            hashlib.sha256(),
            hashlib.sha512(),
        ]

    def update(self, block):
        for h in self.hashers:
            h.update(block)

    @property
    def digests(self):
        return {
            h.name: h.hexdigest()
            for h in self.hashers
        }
